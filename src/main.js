import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import 'nprogress/nprogress.css'
// import BaseIcon from '@components/BaseIcon' //manualy
//1. for import component globall by automaticly
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import 'vue-select/dist/vue-select.css'
import Vuelidate from 'vuelidate'
import DateFilter from './filters/date'
import './registerServiceWorker'

Vue.use('date', DateFilter)
Vue.use(Vuelidate)

Vue.config.productionTip = false
//1.1
const requireComponent = require.context(
  './components', //path to search directory
  false, //subdirectory will not be search
  /Base[A-Z]\w+\.(vue|js)$/ //regex for search Base ending .vue or .js
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1'))
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
}) //end

// Vue.component('BaseIcon', BaseIcon) //manualy

new Vue({
  router,
  store,
  // BaseIcon, //manualy
  render: function(h) {
    return h(App)
  }
}).$mount('#app')
