export const formFieldMixin = {
  inheritAttrs: false,
  props: {
    label: {
      type: String,
      default: ''
    },
    value: [String, Number]
  },
  methods: {
    updatedValue(event) {
      this.$emit('input', event.target.true)
    }
  }
}
