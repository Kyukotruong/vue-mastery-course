import Vue from 'vue'
import VueRouter from 'vue-router'
import EventList from './views/EventList'
import EventShow from './views/EventShow'
import EventCreate from './views/EventCreate'
import User from './pages/user'
import NotFoundComponent from './views/NotFound'
import NProgress from 'nprogress'
import store from '@/store/store'
import NetworkIssue from './views/NetworkIssue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'event-list',
    component: EventList,
    props: true
    // same with
    //props: route => ({
    //   page: route.params.page,
    //   perPage: route.params.perPage,
    //   events: route.params.events,
    //   eventTotal: route.params.eventTotal
    // })
  },
  {
    path: '/event/:id',
    name: 'event-show',
    component: EventShow,
    props: true, // Set params to props
    beforeEnter(routeTo, routeFrom, next) {
      store
        .dispatch('event/fetchEvent', routeTo.params.id)
        .then(event => {
          routeTo.params.event = event
          next()
        })
        .catch(error => {
          if (error.response && error.response.status === 404) {
            next({
              name: '404',
              params: { resource: 'event' }
            })
          } else {
            next({ name: 'network-issue' })
          }
        })
    }
  },
  {
    path: '/event/create',
    name: 'event-create',
    component: EventCreate
  },
  {
    path: '/user/:username',
    name: 'user',
    component: User,
    props: true
  },
  {
    path: '/404',
    name: '404',
    component: NotFoundComponent,
    props: true
    // I added this so we can receive the param as a prop
  },
  {
    path: '*',
    redirect: { name: '404', params: { resource: 'page' } }
  },
  {
    path: '/network-issue',
    name: 'network-issue',
    component: NetworkIssue
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((routeTo, routeFrom, next) => {
  NProgress.start()
  next()
})

router.afterEach(() => {
  NProgress.done()
})
export default router
