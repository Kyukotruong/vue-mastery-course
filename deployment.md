### Deployment Intruction

# Create your Heroku App

```
heroku create <YOUR-PROJECT-NAME-HERE>
heroku config:set NODE_ENV=production --app <YOUR-PROJECT-NAME-HERE>
```

# Create a server.js and Build Your Site

```
npm install express --save
```

# Now add a server.js file to your project’s root directory:

```
// server.js
var express = require('express');
var path = require('path');
var serveStatic = require('serve-static');
app = express();
app.use(serveStatic(__dirname + "/dist"));
var port = process.env.PORT || 5000;
app.listen(port);
console.log('server started '+ port);
```

# IMPORTANT:

`What you probably noticed is that this will serve up a dist directory. dist is a predefined directory that Vue.js builds which is a compressed, minified version of your site. We’ll build this and then tell Heroku to run server.js so Heroku hosts up this dist directory:`

```
npm run build
```

# modify package.json

```
// package.json
{
  "name": "<YOUR-PROJECT-NAME-HERE>",
  "version": "1.0.0",
  "description": "A Vue.js project",
  "author": "",
  "private": true,
  "scripts": {
    "dev": "node build/dev-server.js",
    "build": "node build/build.js",
    "start": "node server.js",   <--- EDIT THIS LINE HERE
...
```

# Git Init and Add Your Heroku Remote Repository

```
#
git init

#
heroku git:remote --app <YOUR-PROJECT-NAME-HERE>

#.gitigore

```

.DS*Store
node_modules/
dist/ <--- REMOVE THIS LINE
npm-debug.log*
yarn-debug.log*
yarn-error.log*
test/unit/coverage
test/e2e/reports
selenium-debug.log
Editor directories and files
.idea
_.suo
_.ntvs\_
_.njsproj
_.sln

```
#
git add . && git commit -a -m "Adding files."
```

# Push Your Code to Deploy!

```
git push heroku master
```
