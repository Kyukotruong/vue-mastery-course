# Have Done Mastering Vuex

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## run server

```
# run server
json-server -d 1500 --watch db.json

-d 1500 add a 1.5(1500 milisecond) second delay before returning data

# stop server
netstat -lnp | grep 3000
kill -9 5351
```

## Dependencies

```

# Date Picker
npm install vuejs-datepicker --save

# Select By Search
npm install vue-select

#
npm install --save-dev sass-loader node-sass

#
npm install lodash --save

# for deployment
npm install express --save

#
npm install -g json-server

```

## ✨ I'am Learned Of :

- Module **Vuex** :
  - State
  - Getters
  - Mutations
  - Actions
  - Modules
- Module **Vue** :
  - Computed
  - Methods
  - Directive
  - Router
  - Single Component
  - Reusable Component

## 🚀 Live Demo

Live Documentation Here [Demo Vue](https://real-world-vue-done.herokuapp.com/ 'Events Project App').

## 🚀 Demo Image

![Demo](/public/Demo.png 'Events Project App')

## 🚩 Licence

Events is open-sourced software licensed under the [MIT license](LICENSE.md).
